# -*- coding: utf-8 -*-

import json
import base64
import logging
import os
import struct
import gzip
from StringIO import StringIO

from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives import hashes, padding, hmac
from cryptography.hazmat.backends import default_backend
import tornado.ioloop
import tornado.web


def derivekey(key, label, context, keyLengthInBits):
    lblcnt = 0 if None == label else len(label)
    ctxcnt = 0 if None == context else len(context)
    buffer = ['\x00'] * (4 + lblcnt + 1 + ctxcnt + 4)
    if lblcnt != 0:
        buffer[4:(4 + lblcnt)] = label
    if ctxcnt != 0:
        buffer[(5 + lblcnt):(5 + lblcnt + ctxcnt)] = context
    _writeuint(keyLengthInBits, buffer, 5 + lblcnt + ctxcnt)
    dstoffset = 0
    v = keyLengthInBits / 8
    res = ['\x00'] * v
    num = 1
    while v > 0:
        _writeuint(num, buffer, 0)
        h = hmac.HMAC(key, hashes.SHA512(), backend=default_backend())
        h.update(''.join(buffer))
        hash = h.finalize()
        cnt = min(v, len(hash))
        res[dstoffset:cnt] = hash[0:cnt]
        dstoffset += cnt
        v -= cnt
        num += 1
    return ''.join(res)


def _writeuint(v, buf, offset):
    buf[offset:(offset + 4)] = struct.pack('>I', v)


def _tokendecode(aspnetstr):
    if len(aspnetstr) < 1:
        raise ValueError('Invalid input')

    # add padding if necessary - last character of the string defines the padding length
    num = ord(aspnetstr[-1]) - 48
    if num < 0 or num > 10:
        return None

    return base64.urlsafe_b64decode(aspnetstr[:-1] + num * '=')


def _decode(aspnetstr):
    # add padding if necessary
    pad = 3 - ((len(aspnetstr) + 3) % 4)
    if pad != 0:
        aspnetstr += pad * '='
    return base64.urlsafe_b64decode(aspnetstr)


def decrypt(dkey, b):
    # extract initialization vector (256 bit)
    iv = b[0:16]
    decryptor = Cipher(algorithms.AES(dkey), modes.CBC(iv), backend=default_backend()).decryptor()
    unpadder = padding.PKCS7(algorithms.AES.block_size).unpadder()

    ciphertext = b[16:-32]
    text_padded = decryptor.update(ciphertext) + decryptor.finalize()
    return unpadder.update(text_padded) + unpadder.finalize()


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write(json.dumps({'status': 'ok'}))
        self.clear_header('Content-Type')
        self.add_header('Content-Type', 'application/json')
        self.finish()


class DecodeHandler(tornado.web.RequestHandler):
    def post(self, *args, **kwargs):
        data = json.loads(self.request.body)
        skey = os.environ.get('SKEY')
        skey = skey.decode('hex')
        enctype = os.environ.get('ENCTYPE')
        aspnetstr = str(data['aspnetstr'])

        label = None
        context = None
        compressed = False
        encrypted = None
        if enctype == 'owinauth':
            label = b'>Microsoft.Owin.Security.Cookies.CookieAuthenticationMiddleware\x11ApplicationCookie\x02v1'
            context = b'User.MachineKey.Protect'
            compressed = True
            encrypted = _decode(aspnetstr)
        elif enctype == 'antiforgery':
            label = b'/System.Web.Helpers.AntiXsrf.AntiForgeryToken.v1'
            context = b'User.MachineKey.Protect'
            encrypted = _tokendecode(aspnetstr)

        dkey = derivekey(skey, context, label, 256)
        decrypted = decrypt(dkey, encrypted)

        if compressed:
            decrypted = gzip.GzipFile(fileobj=StringIO(decrypted)).read()
        self.write(json.dumps({
            'decrtypted_hex': decrypted.encode('hex'),
            'decrypted': decrypted,
        }, ensure_ascii=False))
        self.clear_header('Content-Type')
        self.add_header('Content-Type', 'application/json')
        self.finish()


def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
        (r"/decode", DecodeHandler),
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
