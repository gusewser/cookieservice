FROM python:2.7.15-alpine3.8
RUN apk add --update alpine-sdk libffi-dev build-base py2-pip python2-dev libressl-dev
ADD requirements.txt /requirements.txt
ADD tornado_server.py /tornado_server.py
RUN pip install -r requirements.txt
CMD ["python", "/tornado_server.py"]